# EntregaTP4

## README Ej11 - TP4

Este repositorio contiene un programa en C que implementa un sistema de productores y consumidores utilizando hilos, señales y mutex. El programa lee datos de un archivo de entrada (`pg2000.txt`), procesa estos datos mediante dos productores (productor1 y productor2), y escribe el resultado en un archivo de salida (`salidas.txt`) mediante un consumidor. La comunicación entre los hilos se realiza mediante buffers ping pong (`buffer1` y `buffer2`).


# Funcionalidad

El programa utiliza dos tipos de hilos:

- Productores [Productor1 y Productor2]: Lee líneas del archivo de entrada y escribe en el buffer correspondiente.
- Consumidor: Lee datos de buffer1 y buffer2, y escribe el resultado en el archivo de salida.

# El programa utiliza señales para controlar el flujo de ejecución:

- SIGTERM: Envia una señal para terminar la ejecución del programa.
- SIGUSR1: Cambia entre los dos productores.

# Archivos de Entrada y Salida

- Archivo de Entrada (pg2000.txt): Este archivo contiene los datos que los productores procesarán y el consumidor escribirá en el archivo de salida.
- Archivo de Salida (salidas.txt): Este archivo contendrá el resultado final del procesamiento.
